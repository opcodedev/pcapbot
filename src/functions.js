// configuration data
var config_data = require('../main').config_data;

exports.setColor = setColor;
function setColor(color, str) {
    if(!color) return str;
    ansi_str = "\033[" + color + ";1m" + str + "\033[" + config_data.config.colors.reset + ";0m";
    return ansi_str;
}

exports.setColorNoReset = setColorNoReset;
function setColorNoReset(color, str) {
    if(!color) return str;
    ansi_str = "\033[" + color + ";1m" + str;
    return ansi_str;
}
