// configuration data
var config_data = require('../main').config_data;

// inherit our event emitter from main.js
var ee = require("../main").ee;

// irc lib
var irc = require("irc");

// create the bot
var bot = new irc.Client(config_data.config.irc.server, config_data.config.irc.botName, {
    userName: config_data.config.irc.userName,
    realName: config_data.config.irc.realName,
    port: config_data.config.irc.port,
    debug: config_data.config.irc.debug,
    showErrors: config_data.config.irc.showErrors,
    autoRejoin: config_data.config.irc.autoRejoin,
    autoConnect: config_data.config.irc.autoConnect,
    channels: config_data.config.irc.channels,
    secure: config_data.config.irc.secure,
    selfSigned: config_data.config.irc.selfSigned,
    certExpired: config_data.config.irc.certExpired,
    floodProtection: config_data.config.irc.floodProtection,
    floodProtectionDelay: config_data.config.irc.floodProtectionDelay,
    stripColors: config_data.config.irc.stripColors,
    sasl: config_data.config.irc.sasl,
    localAddress: config_data.config.irc.localAddress,
    channelPrefixes: config_data.config.irc.channelPrefixes,
    messageSplit: config_data.config.irc.messageSplit,
    encoding: config_data.config.irc.encoding
});

// listen for any messages in the channels
bot.addListener("message", function(from, to, text, message) {
    var index = text.indexOf(config_data.config.irc.botName + ":");
    if (index != -1) {
        if (from != config_data.config.irc.botAdmin) {
            bot.say(from, "You actions are being forwarded to admin. Quit hacking!");
            bot.say(config_data.config.irc.botAdmin, from + " tried to execute command -> " + text);
        }

        if (from == config_data.config.irc.botAdmin) {
            if (text == config_data.config.irc.botName + ': exit') {
                process.exit(1);
            }
        }
    }
});

// on registered 'connect' oper up the bot
bot.addListener("registered", function() {
    if (config_data.config.irc.botIsOp === true)
        bot.send (config_data.config.irc.opCmd, config_data.config.irc.botName, config_data.config.irc.opPass);
});

// on BOT/IRC error console.log error message
bot.addListener('error', function(message) {
    console.log('irc error: ', message);
});

// on emit SendIRC relay data to IRC channels
ee.on("sendIRC", function (data) {
    for (var channel in config_data.config.irc.channels) {
        bot.say(config_data.config.irc.channels[channel], data);
    }
});
