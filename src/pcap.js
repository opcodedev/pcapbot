// configuration data
var config_data = require('../main').config_data;

// inherit our event emitter from main.js
var ee = require("../main").ee;

// interface we want to listen on
var interface = config_data.config.pcap.interface;

// our filter type
var filter = config_data.config.pcap.filter;

// our exceptions list used to ignore packets/ips/hosts/flags we are not interested in
var exceptions = config_data.config.pcap.exceptions;

var setColor = require('./functions').setColor;

// pcap wrapper
var pcap = require("pcap"), pcap_session;

function offline() {
    pcap_session = pcap.createOfflineSession(interface,filter);
}

function online() {
    pcap_session = pcap.createSession(interface, filter);

    // Print all devices, currently listening device prefixed with an asterisk
    //console.log("Capture device list: ");
    pcap_session.findalldevs().forEach(function (dev) {
        var ret = "    ";
        if (pcap_session.device_name === dev.name) {
            ret += "* ";
        }
        ret += dev.name + " ";
        if (dev.addresses.length > 0) {
            ret += dev.addresses.filter(function (address) {
                return address.addr;
            }).map(function (address) {
                return address.addr + "/" + address.netmask;
            }).join(", ");
        } else {
            ret += "no address";
        }
        //console.log(ret);
    });

    //console.log();
}

try {
    var stat = require("fs").statSync(interface);
    if (stat && stat.isFile()) {
        offline();
    } else {
        online();
    }
} catch (err) {
    online();
}

// libpcap's internal version number
//console.log(pcap.lib_version);

function rpad(num, len) {
    var str = num.toString();
    while (str.length < len) {
        str += " ";
    }
    return str;
}

var DNSCache = require("./../node_modules/pcap/dns_cache");
var dns_cache = new DNSCache();

var IPv4Addr = require("./../node_modules/pcap/decode/ipv4_addr");
IPv4Addr.prototype.origToString = IPv4Addr.prototype.toString;
IPv4Addr.prototype.toString = function() {
    return dns_cache.ptr(this.origToString());
};


// Listen for packets, decode them, and feed the simple printer.  No tricks.
pcap_session.on("packet", function (raw_packet) {
    var packet = pcap.decode.packet(raw_packet);
    var header = packet.pcap_header;

    var ret = header.tv_sec + "." + rpad(header.tv_usec, 6) + " " + rpad(header.len + "B", 5) + " ";

    //console.log(packet.payload); // console log it for reviewing packet structure

    if (config_data.config.pcap.shortVersion) {
        var print = 1;
        for (var except in exceptions) {
            var index = packet.payload.toString().indexOf(exceptions[except]);// and get its index.
            if(index != -1) {
                print = 0;
                break;
            }
        }

        if (print == 1) {
            var custom_packet = "";
            var pointer = setColor(config_data.config.colors.pointer, " -> ");

            var protoVersion = "";
            if (packet.payload.payload.version == 0)
                setColor(config_data.config.colors.proto, "IPv6");
            if (packet.payload.payload.version == 4)
                protoVersion = setColor(config_data.config.colors.layer3, "IPv4");
            if (packet.payload.payload.version == 43)
                protoVersion = setColor(config_data.config.colors.layer3, "IPv6-Route");
            if (packet.payload.payload.version == 44)
                protoVersion = setColor(config_data.config.colors.layer3, "IPv6-Frag");
            if (packet.payload.payload.version == 58)
                protoVersion = setColor(config_data.config.colors.layer3, "IPv6-ICMP");
            if (packet.payload.payload.version == 59)
                protoVersion = setColor(config_data.config.colors.layer3, "Ipv6-NoNxt");
            if (packet.payload.payload.version == 60)
                protoVersion = setColor(config_data.config.colors.layer3, "Ipv6-Opts");
            custom_packet += protoVersion;

            if (packet.payload.payload.saddr)
                custom_packet += " " + setColor(config_data.config.colors.src, packet.payload.payload.saddr.o1.toString() + "." + packet.payload.payload.saddr.o2.toString() + "." + packet.payload.payload.saddr.o3.toString() + "." + packet.payload.payload.saddr.o4.toString());


            if (packet.payload.payload.daddr)
                custom_packet += pointer + setColor(config_data.config.colors.dst, packet.payload.payload.daddr.o1.toString() + "." + packet.payload.payload.daddr.o2.toString() + "." + packet.payload.payload.daddr.o3.toString() + "." + packet.payload.payload.daddr.o4.toString());

            var protoType = "";
            if (packet.payload.payload.protocol == 6)
                protoType = setColor(config_data.config.colors.layer4, "TCP");
            if (packet.payload.payload.protocol == 17)
                protoType = setColor(config_data.config.colors.layer4, "UDP");
            if (packet.payload.payload.protocol == 1)
                protoType = setColor(config_data.config.colors.layer4, "ICMP");
            custom_packet += " " + protoType;

            if (packet.payload.payload.protocol == 1)
                if (packet.payload.payload.payload.code && packet.payload.payload.payload.code == 3)
                    custom_packet += setColor(config_data.config.colors.src, " Destination Port Unreachable");

            if (packet.payload.payload.payload.sport)
                custom_packet += " " + setColor(config_data.config.colors.dst, packet.payload.payload.payload.sport);
            if (packet.payload.payload.payload.dport)
                custom_packet += pointer + setColor(config_data.config.colors.src, packet.payload.payload.payload.dport);

            /*
             if (packet.payload.payload.payload.seqno)
             custom_packet += setColor(config_data.config.colors.base, " seq: ") + packet.payload.payload.payload.seqno;

             if (packet.payload.payload.payload.ackno)
             custom_packet += setColor(config_data.config.colors.base, " ack: ") + packet.payload.payload.payload.ackno;

             if (packet.payload.payload.payload.flags)
             custom_packet += setColor(config_data.config.colors.base, " flags: ") + packet.payload.payload.payload.flags;

             if (packet.payload.payload.payload.window_size)
             custom_packet += setColor(config_data.config.colors.base, " win: ") + packet.payload.payload.payload.window_size;

             if (packet.payload.payload.payload.checksum)
             custom_packet += setColor(config_data.config.colors.base, " csum: ") + packet.payload.payload.payload.checksum;

             if (packet.payload.payload.length) {
             custom_packet += setColor(config_data.config.colors.base, " len: ") + packet.payload.payload.length;
             } else {
             custom_packet += setColor(config_data.config.colors.base, " len: ") + "0";
             }


            if (packet.payload.payload.payload.data_bytes) {
                custom_packet += setColor(config_data.config.colors.base, " dataBytes: ") + packet.payload.payload.payload.data_bytes;
            } else {
                custom_packet += setColor(config_data.config.colors.base, " dataBytes: ") + "0";
            }
            */

            if (packet.payload.payload.payload.dport && packet.payload.payload.payload.dport == 53)
                if (packet.payload.payload.flags)
                    custom_packet += setColor(config_data.config.colors.base, " DNS: ") + packet.payload.payload.flags.toString();

            //console.log(custom_packet+"\n");
            ee.emit("sendIRC", custom_packet);

        }
    } else {
        ee.emit("sendIRC", packet.payload.toString());
    }
});