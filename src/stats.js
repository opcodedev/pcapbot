// configuration data
var config_data = require('../main').config_data;

// inherit our event emitter from main.js
var ee = require("../main").ee;

// sys lib
var sys = require('sys')

// child process lib
var exec = require('child_process').exec;

// users to watch with lastlog
var users = config_data.config.sysstats.users;

var setColor = require('./functions').setColor;
var setColorNoReset = require('./functions').setColorNoReset;

// on emit stats run commands and emit to IRC bot
ee.on("stats", function () {
    for (var user in users) {
        exec('lastlog | grep '+users[user], puts);
    }
    setTimeout(function() {
        ee.emit("stats2");
    }, 5000);
});

// on emit stats2 run commands and emit to IRC bot
ee.on("stats2", function () {
    setTimeout(function() {
        exec('last -n 5', puts);
        setTimeout(function() {
            ee.emit("stats3");
        }, 5000);
    }, 10000);
});

// on emit stats3 run commands and emit to IRC bot
ee.on("stats3", function () {
    setTimeout(function() {
        exec('netstat -lp | grep -w "LISTEN" | grep -w "tcp"', puts);
    }, 10000);
});

// exec and emit stdout to IRC bot
function puts(error, stdout, stderr) {
    var stats = stdout;

    stats = stats.replace(/tcp/g, setColor(config_data.config.colors.layer4, "TCP"));
    stats = stats.replace(/LISTEN/g, setColor(config_data.config.colors.src, "LISTEN"));
    stats = stats.replace(/\*:/g, setColor(config_data.config.colors.dst, "*:"));
    stats = stats.replace(/pts/g, setColor(config_data.config.colors.dst, "pts"));
    stats = stats.replace(/wtmp begins/g, setColor(config_data.config.colors.details, "wtmp begins"));

    for (var user in users) {
        if (users[user].length > 8) {
            var regExp = new RegExp(users[user].substr(0, 8), "gi");
            stats = stats.replace(regExp, setColorNoReset(config_data.config.colors.src, users[user].substr(0, 8)));
        } else {
            var regExp = new RegExp(users[user], "gi");
            stats = stats.replace(regExp, setColor(config_data.config.colors.src, users[user]));
        }
    }
    ee.emit("sendIRC", stats);
}