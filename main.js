// configuration data
var config_data = require('./config.json');
exports.config_data = config_data;

// EventEmitter using the events lib
var EventEmitter = require("events").EventEmitter;

// create and event emitter
var ee = new EventEmitter();
exports.ee = ee;

// the pcap wrapper
var pcap = require('./src/pcap');

// drop root privileges
// https://thomashunter.name/blog/drop-root-privileges-in-node-js/

// Not used but recommended. How can we do this for interfaces?
// https://thomashunter.name/blog/using-authbind-with-node-js/
try {
    console.log('Old User ID: ' + process.getuid() + ', Old Group ID: ' + process.getgid());
    process.setgid(config_data.config.sysstats.setgid);
    process.setuid(config_data.config.sysstats.setuid);
    console.log('New User ID: ' + process.getuid() + ', New Group ID: ' + process.getgid() + "\n\n");
} catch (err) {
    console.log('Cowardly refusing to keep the process alive as root.'+ "\n\n");
    process.exit(1);
}

// the IRC bot
var bot = require('./src/bot');

// the system stats script
var stats = require('./src/stats');

// lets skip stats on first run
var first_run = 0;

// how often should stats be emitted
var statInt = config_data.config.sysstats.timeout;

// call our stats report every statInt
setInterval(function(){
    if (first_run != 0) {
        ee.emit("stats");
    }
    first_run = 1;
}, statInt);