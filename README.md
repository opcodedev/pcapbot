#PCAPBOT

PCAPBOT is an irc bot for packet and system analysis
for IRC Server Operators and administration helpers.


##System Dependencies

[1] libpcap

```
sudo apt-get install libpcap-dev
```


##NodeJS Dependencies

[1] irc

```
npm install irc
```

[2] pcap

```
npm install pcap
```


##Configuration

See: Rename config.example.json to config.json and make your personal changes.


##Running PCAPBOT

PCAPBOT requires root privileges to access system interfaces.

```
sudo node main.js
```

Notice: root privileges will be dropped back to your user privileges after execution.

See: config.json to set your gid and uid.
